package starter.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ATOCalculatorPage {

    public ATOCalculatorPage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//*[@id=\"ddl-financialYear\"]")
   // @FindBy(xpath = "//*[@id='lbl-financialYear']/following-sibling::div[1]/descendant::select")
    public WebElement incomeYear;

    @FindBy(xpath = "//*[@id='texttaxIncomeAmt']")
    public WebElement taxableIncome;

    @FindBy(xpath = "///*[@id='vrb-resident-span-0']")
    public WebElement residentYear;


    @FindBy(xpath = "//*[@id='bnav-n1-btn4']")
    public WebElement taxCalculateButton;

    @FindBy(xpath = "//div[@class='alert alert-attention']/descendant::p")
    public WebElement calculatedValue;

}

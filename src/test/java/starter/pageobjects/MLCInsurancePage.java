package starter.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MLCInsurancePage {
    public MLCInsurancePage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "(//a[text()='LifeView'])[1]")
    public WebElement lifeViewLink;

    @FindBy(xpath = "//ul[@itemprop='breadcrumb']/descendant::li")
    public List<WebElement> breadcrumbTexts;

    @FindBy(xpath = "//span[text()='Request a demo']/ancestor::a[1]")
    public WebElement requestDemoButton;

    @FindBy(xpath = "//label[contains(., 'Name')]/following-sibling::input[1]")
    public WebElement name;

    @FindBy(xpath = "//label[contains(., 'Company')]/following-sibling::input[1]")
    public WebElement company;

    @FindBy(xpath = "//label[contains(., 'Email')]/following-sibling::input[1]")
    public WebElement email;

    @FindBy(xpath = "//label[contains(., 'Phone')]/following-sibling::input[1]")
    public WebElement phone;

    @FindBy(xpath = "//label[contains(., 'Preferred contact date')]/following-sibling::input[1]")
    public WebElement prefferedContactDate;


    @FindBy(xpath = "//input[@value='AM']")
    public WebElement contactTime;


    @FindBy(xpath = "//label[contains(., 'Request details')]/following-sibling::textarea")
    public WebElement requestDetails;




}

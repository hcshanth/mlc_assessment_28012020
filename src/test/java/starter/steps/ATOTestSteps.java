package starter.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import starter.pageobjects.ATOCalculatorPage;

public class ATOTestSteps {

    WebDriver driver;
    ATOCalculatorPage atoCalculatorPage = new ATOCalculatorPage(driver);


    @Given("^I navigate to ATO Calculator website$")
    public void iNavigateToATOCalculatorWebsite() {
        System.setProperty("webdriver.chrome.driver", "src//test//resources//Drivers//chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC");
        driver.manage().window().maximize();
    }



    @And("^I click on Submit button$")
    public void iClickOnSubmitButton() {
        atoCalculatorPage.taxCalculateButton.click();
    }

    @Then("^I should see the calculated tax for entered details$")
    public void iShouldSeeTheCalculatedTaxForEnteredDetails() {
        String expectedValue = atoCalculatorPage.calculatedValue.getText();
        Assert.assertEquals(expectedValue, "The estimated tax on your taxable income is $31,897.00");
    }

    @When("^I enter \"([^\"]*)\", \"([^\"]*)\"$")
    public void iEnter(String arg0, String arg1) throws Throwable {
        Select select = new Select(atoCalculatorPage.incomeYear);
        select.selectByVisibleText(arg0);

        atoCalculatorPage.taxableIncome.sendKeys(arg1);
    }

    @And("^I select resident type$")
    public void iSelectResidentType() {
        atoCalculatorPage.residentYear.click();
    }


}

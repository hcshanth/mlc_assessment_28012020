package starter.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;

public class AusPostSteps {

    Response response;

    @Given("^I prepare the Auspost Calculate shipping costs API$")
    public void iPrepareTheAuspostCalculateShippingCostsAPI() {
        RestAssured.baseURI = "https://digitalapi.auspost.com.au/postage/parcel/international/calculate?";
    }

    @When("^I submit the API$")
    public void iSubmitTheAPI() {
        response = given().header("auth-key", "5234324").
                                param("country_code", "IN").
                                param("weight", "10").
                                param("service_code", "INT_PARCEL_STD_OWN_PACKAGING").get();

    }

    @Then("^I should see the shipping costs calculated$")
    public void iShouldSeeTheShippingCostsCalculated() {
        Assert.assertEquals(response.getStatusCode(), 200);
    }
}

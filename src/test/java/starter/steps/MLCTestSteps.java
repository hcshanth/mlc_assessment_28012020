package starter.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import starter.pageobjects.MLCInsurancePage;

public class MLCTestSteps {

    WebDriver driver;

    @Given("^I open the MLC insurance website$")
    public void iOpenTheMLCInsuranceWebsite() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src//test//resources//Drivers//chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.mlcinsurance.com.au/");
        driver.manage().window().maximize();
    }

    @When("^I click on lifeview link$")
    public void iClickOnLifeviewLink() throws Throwable {
        MLCInsurancePage mlcInsurancePage = new MLCInsurancePage(driver);
        WebElement element = mlcInsurancePage.lifeViewLink;
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    @Then("^I should see the appropriate breadcrumb$")
    public void iShouldSeeTheAppropriateBreadcrumb() throws Throwable {
        MLCInsurancePage mlcInsurancePage = new MLCInsurancePage(driver);
        String breadCrumbText = "";
        Thread.sleep(5000);
        for(WebElement element : mlcInsurancePage.breadcrumbTexts){
            breadCrumbText = breadCrumbText + element.getText();
        }
        Assert.assertEquals("HomePartnering with usSuperannuation fundsLifeView", breadCrumbText);
    }

    @Then("^I click on Request a Demo link$")
    public void iClickOnRequestADemoLink() throws Throwable {
        MLCInsurancePage mlcInsurancePage = new MLCInsurancePage(driver);
        mlcInsurancePage.requestDemoButton.click();
    }

    @Then("^I should see the demo request details and able to fill the form details$")
    public void iShouldSeeTheDemoRequestDetailsAndAbleToFillTheFormDetails() throws Throwable {
        MLCInsurancePage mlcInsurancePage = new MLCInsurancePage(driver);
        mlcInsurancePage.name.sendKeys("Demo name");
        mlcInsurancePage.company.sendKeys("Demo Company");
        mlcInsurancePage.email.sendKeys("demo@demo.com");
        mlcInsurancePage.phone.sendKeys("0412345678");
        mlcInsurancePage.prefferedContactDate.sendKeys("27-Jan-2020");
        mlcInsurancePage.contactTime.click();
        mlcInsurancePage.requestDetails.sendKeys("This is a demo request");
    }

    @Then("^I close the browser window$")
    public void iCloseTheBrowserWindow() throws Throwable {
        driver.quit();
    }


}

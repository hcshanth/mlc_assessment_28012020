package starter.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import starter.steps.MLCTestSteps;

@RunWith(SerenityRunner.class)
@Narrative(text={"Life view request a demo"})
public class MLCLifeViewTest {

    @Steps
    MLCTestSteps steps;

    @Test
    public void lifeViewTest() throws Throwable {
        steps.iOpenTheMLCInsuranceWebsite();
        steps.iClickOnLifeviewLink();
        steps.iShouldSeeTheAppropriateBreadcrumb();
        steps.iClickOnRequestADemoLink();
        steps.iShouldSeeTheDemoRequestDetailsAndAbleToFillTheFormDetails();
        steps.iCloseTheBrowserWindow();
    }
}

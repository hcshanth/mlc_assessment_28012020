package starter.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import starter.steps.ATOTestSteps;

@RunWith(SerenityRunner.class)
@Narrative(text={"Life view request a demo"})
public class AtoTaxCalcTest {

    @Steps
    ATOTestSteps steps;

    @Test
    public void atoTaxCalc() throws Throwable {
        steps.iNavigateToATOCalculatorWebsite();
        steps.iEnter("2018-19", "120000");
        steps.iSelectResidentType();
        steps.iClickOnSubmitButton();
        steps.iShouldSeeTheCalculatedTaxForEnteredDetails();

    }
}

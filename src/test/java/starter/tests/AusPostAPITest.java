package starter.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import starter.steps.ATOTestSteps;
import starter.steps.AusPostSteps;

@RunWith(SerenityRunner.class)
@Narrative(text={"Life view request a demo"})
public class AusPostAPITest {

    @Steps
    AusPostSteps steps;

    @Test
    public void atoTaxCalc() throws Throwable {
        steps.iPrepareTheAuspostCalculateShippingCostsAPI();
        steps.iSubmitTheAPI();
        steps.iShouldSeeTheShippingCostsCalculated();
    }
}

@test
Feature: MLC Insureance Lifeview Request a demo
  Scenario: Request a demo
    Given I open the MLC insurance website
    When I click on lifeview link
    And I should see the appropriate breadcrumb
    And I click on Request a Demo link
    Then I should see the demo request details and able to fill the form details
    And I close the browser window
@test
Feature: Calculate the Tax from ATO calculator

  Scenario Outcome:
    Given I navigate to ATO Calculator website
    When I enter "<$financial_year>", "<$taxable_income>"
    And I select resident type
    And I click on Submit button
    Then I should see the calculated tax for entered details

  Example:

  | $financial_year |  | $taxable_income |
  | "2018-19" |  | "120000" |
  | "2017-18" |  | "150000" |
  | "2016-17" |  | "80000" |

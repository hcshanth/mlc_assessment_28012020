@test
Feature: Calculate the postage costs of Australia Post for 3 countries
  Scenario:
    Given I prepare the Auspost Calculate shipping costs API
    When I submit the API
    Then I should see the shipping costs calculated
